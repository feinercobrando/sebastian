import React from 'react'
import ContactLayout from '../layouts/ContactLayout'
import Head from 'next/head'
export default function Contact() {
    return (
        <>            
            <Head>
                <title>Sebaztian Maranta | Contact</title>
                <meta name="description" content="TATUADOR y CANTAUTOR Colombiano, mas conocido como KE$LA, especializado en el trabajo de Black and Gray. Ha dejado su marca en personas reconocidas como Justise Winslow (Miami Heat), Jabaal Sheard (Giants), Nejo el Broko, Carolina Lopez, entre otros."/>
                </Head>
            <ContactLayout />
        </>
    )
}
