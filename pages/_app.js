import React, {useState, useMemo, useEffect} from "react"
import {ToastContainer} from "react-toastify"
import AuthContext from "../context/AuthContext"
import {setToken, getToken, removeToken} from '../api/token'
import {useRouter} from 'next/router'
import jwtDecode from 'jwt-decode'
import '../scss/global.scss'
import 'semantic-ui-css/semantic.min.css'
import 'sweetalert2/src/sweetalert2.scss'
import 'react-toastify/dist/ReactToastify.css';
import Head from "next/head";


export default function MyApp({ Component, pageProps }) {
  const [auth, setAuth] = useState(undefined)
  const [reloadUser, setReloadUser] = useState(false)
  const router = useRouter()

  useEffect(() => {
    const token = getToken()
    if(token){
      setAuth({
        token,
        idUser: jwtDecode(token).id
      }) 
    }else{
      setAuth(null)
    }
    setReloadUser(false)
  }, [reloadUser])


  const login = (token) =>{
    setToken(token)
    setAuth({
      token,
      idUser: jwtDecode(token).id
    }) 
  }

  const logout = () =>{
    if(auth){
      removeToken()
      setAuth(null)
      router.push("/")
    }
  }

  const authData = useMemo(
    () => ({
      auth,
      login: login,
      logout,
      setReloadUser,
    }),
    [auth]
  )
  
  if(auth === undefined)return null

  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
        <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
        />
      </Head>
      
      <AuthContext.Provider value={authData}>
        <Component {...pageProps} />
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar
          newestOnTop
          closeOnClick
          rtl={false}
          pauseOnFocusLoss={false}
          draggable
          pauseOnHover
        />
      </AuthContext.Provider>
    </>
  )
  
}

