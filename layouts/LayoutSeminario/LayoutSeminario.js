import React from 'react'
import {Container} from 'semantic-ui-react'
import Card from '../../components/Card/Card';
import Footer from '../../components/Footer';
import TopBar from '../../components/Header/TopBar';
import Seminar from '../../components/Seminario';
import Incluido from '../../components/Seminario/Incluido';
import Virtual from '../../components/Virtual';

export default function LayoutSeminario(props) {
    const {children} = props;
    return (
        <Container fluid >
            <TopBar/> 
            <Seminar/>
            <Incluido/>
            <Card/>
            <Virtual/>
            <Footer/>
            
{/*             <Container  className="content">
                {children}
            </Container> */}
        </Container>
    )
}