import React from 'react'
import {Container} from 'semantic-ui-react'
import Contact from '../../components/Contact/Contact';
import TopBar2 from '../../components/Header/TopBar2';
import Footer from '../../components/Footer/Footer'
export default function ContactLayout() {

    return (
        <Container fluid>
            <TopBar2/>
            <Contact/>
            <Footer/>
        </Container>
    )
}
