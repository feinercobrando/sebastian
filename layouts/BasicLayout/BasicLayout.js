import React from 'react'
import classNames from 'classnames'
import {Container} from 'semantic-ui-react'
import TopBar from '../../components/Header/TopBar';
import Slider from '../../components/Slider/Slider'
import Menu from '../../components/Header/Menu';
export default function BasicLayout(props) {
    const {children, className} = props;
    //className="basic-layout"
    return (
        <Container 
            fluid 
            className={classNames("basic-l ayout", {
            [className]: className,
        })}>
            <TopBar/>
            <Menu/>
            <Container className="content">{children}</Container>
        </Container>
    )
}
