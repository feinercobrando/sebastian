import React from 'react'
import {Container} from 'semantic-ui-react'
import Biografia from '../../components/Biografia/Biografia';
import TopBar from '../../components/Header/TopBar';

import Shade from '../../components/Shade';
import Slider from '../../components/Slider'
import Video2 from '../../components/Video2';
import Youtube from '../../components/Youtube';
import Card from '../../components/Card/Card';
import Footer from '../../components/Footer'
import Pieces from '../../components/Pieces';

export default function LayoutHome(props) {
    const {children} = props;
    return (
        <Container fluid >
            <TopBar/>
            <Slider/>
            <Youtube/>
            <Shade/>
            <Video2/>
            <Biografia/>
            <Card/>
            <Pieces>
                {children}
            </Pieces>
            <Footer/>
            
{/*             <Container  className="content">
                {children}
            </Container> */}
        </Container>
    )
}
