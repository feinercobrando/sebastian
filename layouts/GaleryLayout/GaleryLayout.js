import React from 'react'
import {Container} from 'semantic-ui-react'
import TopBar from '../../components/Header/TopBar';
import Footer from '../../components/Footer/Footer'
import Gallery from '../../components/Galery/Gallery';
export default function GaleryLayout() {

    return (
        <Container fluid>
            <TopBar/>
            <Gallery/>
            <Footer/>
        </Container>
    )
}