import React from 'react'

export default function Video2() {

    const url = "https://tienda-sebastian.s3.us-east-2.amazonaws.com/static/CLIP2.mov"
    return (
        <div className="Video2">
            <video loop autoPlay muted>
                <source src={url}/>
            </video>
        </div>
    )
}