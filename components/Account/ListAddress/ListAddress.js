import React, {useState, useEffect} from 'react'
import { getAddresssApi, deleteAddressApi } from '../../../api/address'
import {Grid, Button} from "semantic-ui-react"
import useAuth from '../../../hooks/useAuth'
import {map, size} from "lodash"

export default function ListAddress(props) {
    const {reloadAddresses, setReloadAddresses, openModal } = props
    const [addresses, setAddresses] = useState(null)
    const {auth, logout} = useAuth()
    console.log(addresses);
    useEffect(() => {
        (async () => {
            const response = await getAddresssApi(auth.idUser, logout)
            setAddresses(response || [] )
            setReloadAddresses(false)
        })()
    
    }, [reloadAddresses])
    if(!addresses) return null
    return (
        <div className="list-address">
            {size(addresses) === 0 ? (
                <h3>no hay ninguna direccion creada </h3>
            ) : (
                <Grid>
                    {map(addresses, (address)=> (
                        <Grid.Column key={address.id} mobile={16} table={8} computer={4}>
                            <Address 
                                address={address} 
                                logout={logout} 
                                setReloadAddresses={setReloadAddresses}
                                openModal={openModal}
                            />
                        </Grid.Column>
                    ))}
                </Grid>
            ) }
        </div>
    )
}


function Address(props) {

    const {setReloadAddresses, openModal, address, logout} = props
    const [loadingDelete, setLoadingDelete] = useState(false)

    const deleteAddress =  async() =>{
        setLoadingDelete(true)
        const response = await deleteAddressApi(id, logout)
        if(response) setReloadAddresses(true)
        setLoadingDelete(false)
    }
    return (
        <div className="address">
            <p>{address.title}</p>
            <p>{address.name}</p>
            <p>{address.address}</p>
            <p>
                {address.state},{address.city} {address.postalCode}
            </p>
            <p>{address.phone}</p>
            <div className="actions">
                <Button primary onClick={() => openModal(`Editar: ${address.title}`, address)}>Edit</Button>
                <Button onClick={deleteAddress} loading={loadingDelete}>Delete</Button>
            </div>
        </div>
    )
}