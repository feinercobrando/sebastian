import React from 'react'
import {Container, Grid, Column} from 'semantic-ui-react'
export default function Seminar() {
    return (
        <div className="seminario">
            <div className="contenedor">
                <div className="caja">
                <Container>
                    <Grid>
                        <Grid.Column className="" width={16}>
                            <div className="whipshade">
                                <img src="/Vector2.png" className="hr1 animate__animated animate__backInLeft"/>
                                    <h1 className="animate__animated animate__flipInX">WHIP SHADE</h1>
                                <img src="/Vector1.png" className="hr2 animate__animated animate__backInRight"/>
                            </div>
                            <h2 className="animate__animated animate__backInUp">VIRTUAL SEMINAR</h2>
                        </Grid.Column>
                    </Grid>
                </Container>
                </div>
            </div>
            <Grid.Column  width={16}>
                <div className="caja2">
                    <div className="centrar">
                        <h2>DALE PLAY A ESTE PRIMER VIDEO Y CONOCE MAS SOBRE ESTE GRAN SEMINARIO!</h2>
                    </div>
                    <div className="video">
                    <iframe className="vhvideo" src="https://www.youtube.com/embed/8BI3JSgtCxE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div className="started">
                        <a>GET STARTED</a>
                    </div>
                </div>
            </Grid.Column>
        </div>
    )
}
