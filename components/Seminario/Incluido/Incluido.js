import React from 'react'
import {Container, Grid, Column} from 'semantic-ui-react'
import Accordion from '../Incluido/Accordion'

export default function Incluido() {

    const composicion = "https://tienda-sebastian.s3.us-east-2.amazonaws.com/static/seminario/COMPOSICION.mov"
    const teoria = "https://tienda-sebastian.s3.us-east-2.amazonaws.com/static/seminario/TEORIA.mov"
    const procreate ="https://tienda-sebastian.s3.us-east-2.amazonaws.com/static/seminario/PROCREATE.mov"
    const mesa = "https://tienda-sebastian.s3.us-east-2.amazonaws.com/static/seminario/MESA+DE+TRABAJO.mov"
    const stencil = "https://tienda-sebastian.s3.us-east-2.amazonaws.com/static/seminario/stencil.mp4"
    const fake = "https://tienda-sebastian.s3.us-east-2.amazonaws.com/static/seminario/FAKEFLESH.mov"
    const tattoo = "https://tienda-sebastian.s3.us-east-2.amazonaws.com/static/seminario/TATTOO.mov"
    const curacion ="https://tienda-sebastian.s3.us-east-2.amazonaws.com/static/seminario/CURACION.mov"
    const fotografia = "https://tienda-sebastian.s3.us-east-2.amazonaws.com/static/seminario/FOTOGRAFIA.mov"
    return (
        <div className="incluido">
            <Container fluid>
                <Grid.Column className="" width={16}>
                    <div className="caja1" >
                        <h1>¿QUE ESTA INCLUIDO EN EL SEMINARIO?</h1>
                        <p>Veremos un proceso completo desde cero, con las bases necesarias para desarrollar un trabajo profesional y entender el trabajo de la piel con las herramientas y el conocimiento fundamental e inspiracional, partiendo desde lo mas importante que es la composicion de diseno, adecuacion de maquinas para la comodidad de cada artista, procesos de WHIP SHADE en procreate, trabajo sobre piel sintetica, sesiones de tattoo en tiempo real y como lograr una curacion perfecta, tambien esta incluido un curso de fotografia basica enfocada en el tatuaje, edicion de fotografias y exportacion para redes sociales.</p>
                        <p>Es un seminario bastante completo para que logres expresar tu arte de la mejor manera y con los mejores simientos para que evites tantos dolores de cabeza y estres intentando aprender por tu propia cuenta, lo ideal es evolucionar el arte y la industria de tatuaje con todo el conocimiento necesario.</p>
                        <h3>TODO ESTE PROYECTO LO HE DIVIDIDO EN 9 MODULOS, LOS CUALES ESTARAN DISPONIBLES CADA 15 DIAS INICIANDO EL 11 DE JUNIO.</h3>
                        <h3>¿DE QUE ESTA COMPUESTO CADA MODULO?</h3>

                    </div>

                </Grid.Column>
            </Container>
            <div className="App">
                <div className="diseno">
                    <div className="acordion_diseno">
                        <Accordion title="COMPOSICION DE DISEÑO">
                                <video loop autoPlay muted>
                                    <source src={composicion}/>
                                </video>
                                <p>Herramientas de trabajo</p>
                                <p>Partes del cuerpo y ubicación</p>
                                <p>Seleccion de referencias</p>
                                <p>Ajustes de imágenes</p>
                                <p>Contraste y composición</p>
                                <h5>JUNIO 11</h5>
                        </Accordion>
                    </div>
                </div>
                <div className="diseno">
                    <div className="acordion_diseno">
                        <Accordion title="TEORIA Y MATERIALES">
                                <video loop autoPlay muted>
                                    <source src={teoria}/>
                                </video>
                            <p>Clases de agujas y tamaños</p>
                            <p>Velocidades de recorrido</p>
                            <p>Tipos de máquina y adaptación</p>
                            <p>Tipos de punteado y formas</p>
                            <p>Manejo de tintas y dilución</p>
                            <p>Orden de aplicación</p>
                            <p>Linea de guia</p>
                            <p>Difuminado de tonos por capas</p>
                            <p>Contraste y volumen</p>
                            <p>Inyección blanco y negro</p>
                            <h5>JUNIO 25</h5>
                        </Accordion>
                    </div>
                </div>
                <div className="diseno">
                    <div className="acordion_diseno">
                        <Accordion title="WHIP SHADING (PROCREATE)">
                                <video loop autoPlay muted>
                                    <source src={procreate}/>
                                </video>
                                <p>Conociendo el whip shade</p>
                            <p>Configuración de pinceles</p>
                            <p>Velocidad y recorrido</p>
                            <p>Sombreado en lineas y circulos</p>
                            <p>Trabajo de volumenes</p>
                            <h5>JULIO 9</h5>
                        </Accordion>
                    </div>
                </div>
                <div className="diseno">
                    <div className="acordion_diseno">
                        <Accordion title="MESA DE TRABAJO">
                            <video loop autoPlay muted>
                                <source src={mesa}/>
                            </video>
                            <p>Bioseguridad</p>
                            <p>Materiales</p>
                            <p>Mesa de trabajo</p>
                            <h5>JULIO 23</h5>
                        </Accordion>
                    </div>
                </div>
                <div className="diseno">
                    <div className="acordion_diseno">
                        <Accordion title="STENCIL">
                            <video loop type="video/mp4" autoPlay muted>
                                <source src={stencil}/>
                            </video>
                            <p>Plantilla</p>
                            <p>Linea y Sombra de guia</p>
                            <p>Impresión</p>
                            <h5>AGOSTO 6</h5>
                        </Accordion>
                    </div>
                </div>
                <div className="diseno">
                    <div className="acordion_diseno">
                        <Accordion title="FAKE FLESH">
                            <video loop type="video/mp4" autoPlay muted>
                                <source src={fake}/>
                            </video>
                            <p>Stencil</p>
                            <p>Linea guia</p>
                            <p>Tipos de sombreado</p>
                            <p>Difuminados de tonos por capas</p>
                            <p>Contraste y volumen</p>
                            <h5>AGOSTO 20</h5>
                        </Accordion>
                    </div>
                </div>
                <div className="diseno">
                    <div className="acordion_diseno">
                        <Accordion title="TATTOO">
                            <video loop type="video/mp4" autoPlay muted>
                                <source src={tattoo}/>
                            </video>
                            <p>PROCESOS DE TATTOO (TIEMPO REAL)</p>
                            <p>Stencil</p>
                            <p>Linea guia</p>
                            <p>Orden de aplicación</p>
                            <p>Difuminados de tonos por capas</p>
                            <p>Estilos de sombreado</p>
                            <p>Contraste y volumen</p>
                            <p>Inyección Blanco y Negro.</p>
                            <h5>SEPTIEMBRE 3</h5>
                        </Accordion>
                    </div>
                </div>
                <div className="diseno">
                    <div className="acordion_diseno">
                        <Accordion title="CURACIÓN">
                            <video loop type="video/mp4" autoPlay muted>
                                <source src={curacion}/>
                            </video>
                            <p>Cerrar poros </p>
                            <p>Protección para curación</p>
                            <p>Cuidados del tatuaje </p>
                            <p>Recomendaciones</p>
                            <h5>SEPTIEMBRE 17</h5>
                        </Accordion>
                    </div>
                </div>
                <div className="diseno">
                    <div className="acordion_diseno">
                        <Accordion title="FOTOGRAFIA">
                            <video loop type="video/mp4" autoPlay muted>
                                <source src={fotografia}/>
                            </video>
                            <p>Herramientas de fotografia</p>
                            <p>Edicion RAW </p>
                            <p>Photoshop</p>
                            <p>Fotografía final</p>
                            <h5>OCTUBRE 1</h5>
                        </Accordion>
                    </div>
                </div>
                        


            </div>
        </div>
    )
}
