import React from 'react'

export default function Youtube() {

    const url = "https://tienda-sebastian.s3.us-east-2.amazonaws.com/static/CLIP1WEB.mov"
    return (
        <div className="youtube">
            <video loop autoPlay muted>
                <source src={url}/>
            </video>
        </div>
    )
}
