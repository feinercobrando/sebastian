import React, {useState} from 'react'
import {Form, Button} from 'semantic-ui-react'
import {useFormik} from 'formik'
import * as Yup from "yup"
import {toast} from 'react-toastify'
import {registerApi} from '../../../api/user'

export default function RegisterForm(props) {

    const {showLoginForm} = props
    const [loading, setLoading] = useState(false)

    const formik = useFormik({
        initialValues: initialValues(),
        validationSchema: Yup.object(validationSchema()),
        onSubmit: async (formData) =>{ 
            setLoading(true)
            const response = await registerApi(formData)
            if(response?.jwt){
                toast.success("Registro Correcto")
                showLoginForm()
            }
            else {
                toast.error("Error al registrar el usuario, inténtelo mas tarde")
            }
            setLoading(false)
        }
    })
    return (
        <Form className="login-form" onSubmit={formik.handleSubmit}>
            <Form.Input 
                onChange={formik.handleChange}
                error={formik.errors.name}
                name="name"
                type="text"
                placeholder="Name"
            />
            <Form.Input 
                onChange={formik.handleChange}
                error={formik.errors.lastname}
                name="lastname"
                type="text"
                placeholder="Last Name" 
            />
            <Form.Input 
                onChange={formik.handleChange}
                error={formik.errors.username}
                name="username"
                type="text"
                placeholder="Username"
            />
            <Form.Input 
                onChange={formik.handleChange}
                error={formik.errors.email}
                name="email"
                type="text"
                placeholder="Email"
            />
            <Form.Input 
                onChange={formik.handleChange}
                error={formik.errors.password}
                name="password"
                type="password"
                placeholder="Password"
            />
            <div className="actions">
                <Button type="button" basic onClick={showLoginForm}>
                    Log in
                </Button>
                <Button className="submit" type="submit" loading={loading}>Register</Button>
            </div>
        </Form>
    )
}

function initialValues(){
    return{
        name: "",
        lastname: "",
        username: "",
        email: "",
        password: "",

    }
}

function validationSchema(){
    return{
        name: Yup.string().required(true),
        lastname: Yup.string().required(true),
        username: Yup.string().required(true),
        email: Yup.string().email(true).required("Email Required"),
        password: Yup.string().required(true),
    }
}