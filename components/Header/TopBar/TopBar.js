import {Container, Grid, Image,Menu, Input, Icon} from 'semantic-ui-react'
import Link from 'next/link'
import { useState,useEffect} from 'react';
import { Turn as Hamburger } from 'hamburger-react'
import BasicModal from '../../Modal/BasicModal';
import Auth from '../../Auth/Auth';
import useAuth from '../../../hooks/useAuth';
import {getMeApi} from '../../../api/user';



export default function TopBar() {
    const [menu, setMenu] = useState({
        openmenu : false,
        opacity:1,
        Cambionav: 'header',
        menuu: '/menuWhite.svg',
        none: 'none',
        logo: 'https://tienda-sebastian.s3.us-east-2.amazonaws.com/static/logowhite.png'

    })
    const {Cambionav, logo} = menu;

    const [isOpen, setOpen] = useState(false)
    const [showModal, setShowModal] = useState (false)
    const onCloseModal = () =>setShowModal(false)
    const onShowModal = () => setShowModal(true)
    const [titleModal, setTitleModal] = useState('Iniciar sesión')

    const [user, setUser] = useState(undefined)
    const {auth, logout} = useAuth()

    const handleAnimation = () => {
        if(document.documentElement.scrollTop > 80){
            setMenu({ Cambionav : 'fixed', logo: '/LOGOWHITE.png', menuu:'/menublack.svg' })
        } 
        else {
            setMenu({ Cambionav : 'header', logo: '/LOGOWHITE.png', menuu:'/menuWhite.svg' })
        }
    }

    useEffect(() => {
        setMenu(window.onscroll = handleAnimation);
        (async() =>{
            const response = await getMeApi(logout)
            setUser(response)
        })()
    }, [auth])

    
    return (
        <div className="top-bar">

            <Container fluid>
                <Grid className={Cambionav}>

                    <Grid.Column  tablet={5} computer={5} className="top-bar__left none animate__animated animate__fadeIn">
                         {user !== undefined && (
                             <MenuOptions 
                                onShowModal={onShowModal} 
                                user={user} 
                                logout={logout}
                                /> 
                         )}
                         
                    </Grid.Column>                  
                    <Grid.Column  mobile={16} tablet={6} computer={6} className="top-bar__img">
                        <Link href="/">
                            <a><Image className="animate__animated animate__flipInX" src={logo} alt="Sebastian" /></a> 
                        </Link>
                        <Hamburger className="active animate__animated animate__fadeIn" toggled={isOpen} toggle={setOpen} size={20}/>
                            {isOpen ? 
                                <div className='open_menu animate__animated animate__fadeIn'>
                                    <div className='active'>
                                        <div className='list'>
                                        <Link href="/">
                                            <li className="animate__animated animate__flipInX animate__delay-1s"><a>HOME</a></li>
                                        </Link>
                                        <Link href="/seminar">
                                            <li className="animate__animated animate__flipInX animate__delay-1s" ><a>SEMINAR</a></li>
                                        </Link>
                                        <Link href="/gallery">
                                            <li className="animate__animated animate__flipInX animate__delay-1s"  ><a>GALLERY</a></li>
                                        </Link>
                                        <Link href="/contact">
                                            <li className="animate__animated animate__flipInX animate__delay-1s"  ><a>CONTACT</a></li>
                                        </Link>
                                        </div>
                                    </div>
                                </div> 

                                : ''
                            } 
                    </Grid.Column>

                    <Grid.Column  tablet={5} computer={5} className="top-bar__right none animate__animated animate__fadeIn">
                        <MenuPlatforms/>
                    </Grid.Column>
                </Grid>
            </Container>
            <BasicModal 
                show={showModal} 
                title={titleModal}
                setShow={setShowModal}
                
                >

                <Auth onCloseModal={onCloseModal} setTitleModal={setTitleModal}/>
            </BasicModal>
        </div>
    )
}
function MenuPlatforms () {
    return(
        <Menu>
            <Link href="/seminar">
                <Menu.Item className="espacios" as="a">
                    SEMINAR
                </Menu.Item>
            </Link>
            <Link href="/gallery">
                <Menu.Item className="espacios" as="a">
                    GALLERY
                </Menu.Item>
            </Link>
            <Link href="/contact">
                <Menu.Item className="espacios" as="a">
                    CONTACT
                </Menu.Item>
            </Link>
        </Menu>
    )
}

function MenuOptions(props){
    const {onShowModal , user, logout} = props
    return(
        <Menu>
            {user ? (
                <>
                <Link href="/orders">
                    <Menu.Item as="a" className="m-0">
                        <Icon name="unordered list"/>
                       
                    </Menu.Item>
                </Link>
                <Link href="/wishlist" >
                    <Menu.Item as="a" className="m-0">
                        <Icon name="heart outline"/>
                       
                    </Menu.Item>
                </Link>
                <Link href="/account">
                    <Menu.Item as="a" className="m-0">
                        <Icon name="user outline"/>
                        {user.name}
                    </Menu.Item>
                </Link>
                <Link href="/cart">
                    <Menu.Item as="a" className="m-0">
                        <Icon name="cart"/>
                        
                    </Menu.Item>
                </Link>
                <Menu.Item onClick={logout} className="m-0">
                    <Icon name="power off"/>
                    
                </Menu.Item>  
                </>
            ) : ( null
/* 
            <Menu.Item onClick={onShowModal}>
                <Icon name="user outline" />
                MY ACCOUNT
            </Menu.Item> */
            )}

        </Menu>
    )
}

/* function Buscador () {
    return (
        <Input
            id="search-mat"
            icon={{name: "search"}}
            
        />
    )
} */