import {Container, Grid, Image,Menu, Input, Icon} from 'semantic-ui-react'
import Link from 'next/link'
import { useState,useEffect} from 'react';
import { Turn as Hamburger } from 'hamburger-react'


export default function TopBar2() {
    const [menu, setMenu] = useState({
        openmenu : false,
        opacity: 1,
        styleClass: 'header',
        logo: '/LOGOWHITE.png',
        menu: '../../images/iconos/menuWhite.svg',
        none: 'none',

    })

    const [isOpen, setOpen] = useState(false)
    const handleAnimation = () => {

        if(document.documentElement.scrollTop > 80){
            setMenu({ styleClass : 'fixed',   none:'none', logo: '/LOGOWHITE.png', menu:'../../images/iconos/menublack.svg' })
            
        } 
        else {
            setMenu({ styleClass : 'header', logo: '/LOGOWHITE.png', menu:'../../images/iconos/menuWhite.svg' })

        }
  
    }
    useEffect(() => {
        setMenu(window.onscroll = handleAnimation)
    }, [])

    const {styleClass, logo,} = menu;
    return (
        <div className="top-bar2">
            <Container fluid>
                <Grid className={styleClass}>
                    <Grid.Column tablet={5} computer={5}className="top-bar2__left none animate__animated animate__fadeIn">
                        {/* <MenuOptions/> */}
                    </Grid.Column>                  
                    <Grid.Column mobile={16} tablet={6} computer={6} className="top-bar2__img">
                        <Link href="/">
                            <a><Image className="animate__animated animate__flipInX" src={logo} alt="Sebastian" /></a>
                        </Link>
                        <Hamburger className="active animate__animated animate__fadeIn" toggled={isOpen} toggle={setOpen} size={20}/>
                            {isOpen ? 
                                <div className='open_menu animate__animated animate__fadeIn'>
                                    <div className='active'>
                                        {/*  <div className='close' onClick={(e) => {toggle(setOpen)}}>X</div> */}
                                        <div className='list'>
                                        <Link href="/">
                                            <li className="animate__animated animate__flipInX animate__delay-1s"><a>HOME</a></li>
                                        </Link>
                                        <Link href="/seminar">
                                            <li className="animate__animated animate__flipInX animate__delay-1s" ><a>SEMINAR</a></li>
                                        </Link>
                                        <Link href="/gallery">
                                            <li className="animate__animated animate__flipInX animate__delay-1s"  ><a>GALLERY</a></li>
                                        </Link>
                                        <Link href="/contact">
                                            <li className="animate__animated animate__flipInX animate__delay-1s"  ><a>CONTACT</a></li>
                                        </Link>
                                        </div>
                                    </div>
                                </div> 

                                : ''
                            } 
                    </Grid.Column>
                    <Grid.Column tablet={5} computer={5}  className="top-bar2__right none animate__animated animate__fadeIn">
                        <MenuPlatforms/>
                    </Grid.Column>
                </Grid>
            </Container>
        </div>
    )
}
function MenuPlatforms () {
    return(
        <Menu>
            <Link href="/seminar">
                <Menu.Item className="espacios" as="a">
                    SEMINAR
                </Menu.Item>
            </Link>
            <Link href="/gallery">
                <Menu.Item className="espacios" as="a">
                    GALLERY
                </Menu.Item>
            </Link>
            <Link href="/contact">
                <Menu.Item className="espacios" as="a">
                    CONTACT
                </Menu.Item>
            </Link>
        </Menu>
    )
}

function MenuOptions(){
    return(
        <Menu>
            <Menu.Item>
                <Icon name="user outline" />
                MY ACCOUNT
            </Menu.Item>

        </Menu>
    )
}

/* function Buscador () {
    return (
        <Input
            id="search-mat"
            icon={{name: "search"}}
            
        />
    )
} */
