import {Container, Menu, Grid, Icon, Label}from 'semantic-ui-react'
import Link from 'next/link'

export default function MenuWeb() {
    return (
        <div className="menu">
            <Container>
                <Grid>
                    <Grid.Column className="menu__left" width={6}>
                        <MenuPlatforms/>
                    </Grid.Column>
                    <Grid.Column className="menu__right" width={10}>
                        <MenuOptions/>
                    </Grid.Column>
                </Grid>
            </Container>
        </div>
    )
}


function MenuPlatforms () {
    return(
        <Menu>
            <Link href="/playstation">
                <Menu.Item as="a">
                    HOME
                </Menu.Item>
            </Link>
            <Link href="/playstation">
                <Menu.Item as="a">
                    SEMINARY
                </Menu.Item>
            </Link>
            <Link href="/playstation">
                <Menu.Item as="a">
                    GALERY
                </Menu.Item>
            </Link>
            <Link href="/playstation">
                <Menu.Item as="a">
                    CONTACT
                </Menu.Item>
            </Link>
        </Menu>
    )
}

function MenuOptions(){
    return(
        <Menu>
            <Menu.Item>
                <Icon name="user outline" />
                MY ACCOUNT
            </Menu.Item>

        </Menu>
    )
}