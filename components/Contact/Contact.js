import React, { useState, useEffect } from 'react'
import {useForm} from '../hooks/useForm'
import Swal from 'sweetalert2'
import {Container, Input, Grid, Icon, Image, TextArea, Form, PlaceholderLine, Label} from 'semantic-ui-react'
import axios from 'axios'
export default function Contact() {

    const  [formValues, handleInputChange] = useForm({
        name: '',
        phone: '',
        email: '',
        textarea: ''
    })
    
    const {name, phone, email, textarea}= formValues;

    const [image, setImage] = useState('');
    
    const handleSubmit = async (e) => {
        let data ={
            name,
            phone,
            email,
            textarea,
            image,
        }

        const form = await axios.post("https://sebaztianmaranta.herokuapp.com/api/form" , data)
        e.preventDefault()

            Swal.fire({
                icon: 'success',
                type: 'success',
                title: 'Mensaje enviado',
                text:'Gracias por contactar con Sebaztian! responderemos a tu consulta lo mas pronto posible!' ,
                showConfirmButton: false,    
                timerProgressBar: true,                                        
                timer: 4000,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                  }
                })
                
        return form
    }


    const handleFile = ({ target }) =>{
        const byte = 1048576
        const file = target.files[0];
        const tamaño = file.size / byte;

        if (tamaño > 1) {
            console.log('Error')
        } else {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                console.log(reader.result);
                setImage(reader.result);
            }
            reader.onerror = () => console.log(error);
        }

    }


    return (
        <div className="contacto">
            <Container fluid className="alto">
                <Grid verticalAlign="middle" className="alto2" >
                    <Grid.Column mobile={16} tablet={10} computer={10} >
                        <div className="cajaimg">
                            <h2 className="animate__animated animate__zoomIn">Para Consultas porfavor especificar ubicación, tamaño, foto de la parte a ser tatuada y adjuntar imagenes de referencia "no tatuajes"</h2>
                            <Image className="animate__animated animate__zoomIn" src="https://tienda-sebastian.s3.us-east-2.amazonaws.com/static/fondoimage.png"/>
                        </div>

                    </Grid.Column>
                    <Grid.Column mobile={16} tablet={6} computer={6} >
                        <div className="cajacontact">
                            <h1 className="animate__animated animate__zoomIn">CONTACT US</h1>
                            <Form onSubmit={handleSubmit}>
                                    <Input onChange={handleInputChange} value={name} name="name" required fluid iconPosition='left' placeholder='First Name'>
                                    <Icon  name='male' />
                                    <input />
                                    </Input>
                                <br/>
                                    <Input onChange={handleInputChange} value={phone} name="phone"  type="number" required fluid iconPosition='left' placeholder='Phone'>
                                    <Icon  name='phone' />
                                    
                                    <input />
                                    </Input>
                                <br/>
                                    <Input onChange={handleInputChange} value={email} name="email" fluid required iconPosition='left' placeholder='Email'>
                                    <Icon  name='at' />
                                    <input />
                                    </Input>
                                <br/>
                                <TextArea onChange={handleInputChange} value={textarea} name="textarea" required placeholder='¿What can we do for you?' />
                                <br/>
                                <div className="mt2">
                                    <Label  >Imagenes de referencia tamaño máximo 1MD :</Label>
                                    <Input fluid type="file" name="image" onChange={handleFile}></Input>

                                </div>

                                <div className="cajasend">
                                    <button type="submit"  className="send">SEND</button>
                                </div>
                            </Form>
                        </div>
                    </Grid.Column>
                </Grid>
            </Container>
        </div>
    )
}
