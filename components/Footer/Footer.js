import React from 'react'
import {Container, Grid, Column, Image} from 'semantic-ui-react'
import IconFacebook from './IconFacebook'
import IconYoutube from './IconYoutube'
import IconInstagram from './IconInstagram'

export default function Footer() {
    const logo = "https://tienda-sebastian.s3.us-east-2.amazonaws.com/static/logowhite.png"
    return (
        <Container fluid className="footer">
            <Container>
                <Grid>
                    <Grid.Column mobile={16} tablet={5} computer={6}>
                        <div className="primacy">
                            <Image src="https://tienda-sebastian.s3.us-east-2.amazonaws.com/static/seminario/logoprimacy.png"></Image>
                        </div>
                        <br />
                        <div className="logo">
                            <Image src={logo}></Image>
                        </div>
                        <br/>
                        <h3 className="copyright">COPYRIGTH 2021 SEBAZTIAN MARANTA</h3>   
                    </Grid.Column>

                    <Grid.Column mobile={16} tablet={5} computer={5}>
                        <Grid.Row >
                            <div className="h1">
                                <h1>FOLLOW</h1>
                            </div>
                        </Grid.Row>
                        <Grid.Row className="icons" >
                            <Grid.Column mobile={5} tablet={5} computer={5}>
                                <a target="blank" href="https://www.facebook.com/profile.php?id=592607840">
                                    <IconFacebook/>
                                </a>
                            </Grid.Column>
                            <Grid.Column mobile={5} tablet={5} computer={5}>
                                <a target="blank" href="https://www.instagram.com/sebaztian_maranta/">
                                    <IconInstagram/>
                                </a>
                            </Grid.Column>
                            <Grid.Column mobile={5} tablet={5} computer={5}>
                                <a target="blank" href="https://www.youtube.com/channel/UCB3DoFuE1zfi8xEG1BJSs6A">
                                    <IconYoutube/>
                                </a>
                            </Grid.Column>
                        </Grid.Row>
                        
                    </Grid.Column>
                    <Grid.Column mobile={16} tablet={5} computer={5}>
                        <div className="contact">
                            <h1>CONTACT</h1>
                        </div>
                        <div className="correo">
                            <h2>booknow@sebaztianmaranta.com</h2>
                        </div>
                    </Grid.Column>
                </Grid>
            </Container>
        </Container>
    )
}
