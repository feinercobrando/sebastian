import { Grid, GridColumn, Card } from "semantic-ui-react";
import Link from 'next/link'

export default function ImgGridItem ({ caption, url, link}) {

    return (
        <div className="my-card">
            <a href={link} target="_blank">
                    <img  className="img" src={url} alt={link}></img>

                    {/* <div className="profile-position">{caption}</div> */}
            </a>
        </div>

    )
}
