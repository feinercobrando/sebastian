import { useState, useEffect } from "react"
import ImgGridItem from './ImgGridItem'

export default function  Gallery() {


    const [state, setState] = useState({
        images:[],
        load:true
    })
   const {images, load} = state
    const api = 'IGQVJWNlU3NGV3NDU4dkRKRDM1ZAWhSMWNraWZAtbWU3b1kyYTBpeXNQdk8zY0tEWHkxTmJXaWNVN3dVSDZADVXJSci1USHhvdHZAHOWdJc1FkQ0dvV05PMHRzZAXBtRlNvelJnem9mN1hPRVkxNndNTVc1VwZDZD'
    const fetchinstagram = async () =>{
        const url = `https://graph.instagram.com/me/media?fields=id,media_type,thumbnail_url,caption,media_url,permalink&access_token=${api}&limit=140`
        const resp = await fetch(url)
        const {data} = await resp.json()
        console.log(data);
        const imagenes = data.map(img =>{
            return{
                id: img.id,
                url: img.media_type==='VIDEO'?img.thumbnail_url : img.media_url,
                link: img.permalink,
                caption: img.caption,
                type: img.type
            } 
        })
        setState({images:imagenes, load:false})
    }

    useEffect( () =>{
        fetchinstagram()
    }, [])
    


    return (
        <div className="galery">
                {load && 
                    <div className="loading">
                        <div className="loading-text">
                            <h2 className="animate">Loading</h2>
                        </div>
                    </div> 
                }

                <div className="card-group animate__animated animate__fadeIn animate__delay-2s">
                    {
                        images.map(img =>(
                            <ImgGridItem 
                                {...img}
                                key={img.id}
                            />
                        ))
                    }

                </div>

        </div>
    )
}
