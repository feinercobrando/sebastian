import React from 'react'
import Link from 'next/link'

export default function Pieces() {
    return (
        <div className="Pieces">
            <div className="caja">
                <div className="container">
                <h1>CHECK OUT MY LATEST PIECES</h1><br/>
                <Link href="/gallery"><div className="btnBook animate__animated animate__flipInX"> <a >GALLERY</a></div></Link>
                </div>
            </div>
        </div>
    )
}
