import React from 'react'
import {Container, Grid, Column} from 'semantic-ui-react'
export default function Biografia() {
    return (
        <div className="biografia">
            <div className="contenedor">
                <div className="caja">
                <Container>
                    <Grid>
                        <Grid.Column className="" width={16}>
                            <h1>SEBAZTIAN MARANTÁ</h1>
                        </Grid.Column>
                        <Grid.Column className="parrafo" width={16}>
                            <div className="bio">
                                <h2>
                                    TATUADOR y CANTAUTOR Colombiano, mas conocido como KE$LA, especializado 
                                    en el trabajo de Black and Gray. Ha dejado su marca en personas reconocidas como 
                                    Justise Winslow (Miami Heat), Jabaal Sheard (Giants), Nejo el Broko, Carolina Lopez,
                                    entre otros.
                                </h2>
                            </div>
                        </Grid.Column>
                    </Grid>
                </Container>
                </div>
            </div>
        </div>
    )
}
