import AwesomeSlider from 'react-awesome-slider';
import withAutoplay from 'react-awesome-slider/dist/autoplay';
import 'react-awesome-slider/dist/styles.css';

const AutoplaySlider = withAutoplay(AwesomeSlider);

export default function slider() {
    return(
        <>
        <AutoplaySlider
            play={true}
            cancelOnInteraction={false} // should stop playing on user interaction
            interval={4000}
            bullets={false}
            infinite={true}
        >
            <div data-src="/Slider1.jpg" className="z">
                <h1 className="animate__animated animate__flipInX">¿WANT TO GET TATTOOED?</h1>
                <div className="btnBook"><a >BOOK NOW</a></div>
            </div>
            <div data-src="/Slider2.jpg" className="z">
                <h1 >A UNIQUE ART</h1>
            </div>
            <div data-src="/Slider3.jpg" className="z">
                <h3>¿INVEST ON YOUR SKIN?</h3>
                <h1>MAKE IT WITH THE HIGEST QUALITY</h1>
                <div className="btnBook animate__animated animate__flipInX"><a >BOOK NOW</a></div>
            </div>
        </AutoplaySlider>

        </>
)}; 

